# Export DONJON sheet to FoundryVTT actor sheet

Este script de Python consulta el endpoint del generador de fichas de DONJON y en base 
a esta genera un archivo JSON de un Actor para Foundry VTT.

Se toman los valores del JSON de DONJON y se reemplazan en la JSON de ejemplo de Foundry provisto por Viriato139ac.

### Consideraciónes:
Para el reemplazo de las habilidades se tiene un listado en inglés de estas en el código. El script obtiene las habilidades del JSON de DONJON (que vienen en un solo campo de texto) y genera un listado. Por cada uno de estas se van a buscar al listado en inglés, de encontrarse se obtiene la traducción y con el nombre de la habilidad en español se va a buscar el JSON de Foundry y se reemplaza su valor. Ej:

```sh
# De donjon se obtuvieron las siguientes "skills":
[('Credit Rating', '70'), ('Library Use', '80'), ('Psychology', '60'), ('Own Language', '70'), ('Climb', '70'), ('Disguise', '30'), ('Ride', '30')]

# En el código están así:
SKILLS = {
    "Credit Rating": "Crédito",
    "Library Use": "Buscar libros",
    "Psychology": "Psicología",
    "Own Language": "Lengua propia",
    "Climb": "Trepar",
    "Disguise": "Disfrazarse",
    "Ride": "",
    ...
}

# El script muestra las habilidades encontradas y los valores originales y por los que se actualizarán
Crédito: None -> 70
Buscar libros: None -> 80
Psicología: None -> 60
Trepar: None -> 70
Disfrazarse: None -> 30
```

**Nota** En el ejemplo puede notarse que *Ride* no se reemplazó ya que en la ficha de foudry provista no está Cabalgar y además en el listado de habilidades en inglés no está su traducción. Esto se corregirá en una futura versión.

El proceso generará un nuevo archivo json con nombre `fvtt-Actor-<primer_nombre_de_donjon>-<segundo_nombre_de_donjon>.json`