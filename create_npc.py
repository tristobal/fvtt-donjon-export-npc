import requests
import json
import re

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
DONJON_URL = 'https://donjon.bin.sh/weird/coc_npc/rpc.cgi?era=Jazz&gender=&class='
REGEX_SKILL = r'(.*)( \()(\d\d)(%\))'
SKILLS = {
    "Accounting": "Contabilidad",
    "Anthropology": "Antropología",
    "Appraise": "Tasación",
    "Archaeology": "Arqueología",
    "Art / Craft": "",
    "Charm": "Encanto",
    "Climb": "Trepar",
    "Credit Rating": "Crédito",
    "Cthulhu Mythos": "Mitos de Cthulhu",
    "Disguise": "Disfrazarse",
    "Dodge": "Esquivar",
    "Drive Auto": "Conducir automóvil",
    "Electrical Repair": "Electricidad",
    "Fast Talk": "Charlatanería",
    "Fighting (Brawl)": "Combatir (Pelea)",
    "Firearms (Handgun)": "Armas de fuego (Arma corta)",
    "Firearms (Rifle Shotgun)": "Armas de fuego (Fusil/Escopeta)",
    "First Aid": "Primeros auxilios",
    "History": "Historia",
    "Intimidate": "Intimidar",
    "Jump": "Saltar",
    "Language (Other)": "",
    "Own Language": "Lengua propia",
    "Law": "Derecho",
    "Library Use": "Buscar libros",
    "Listen": "Escuchar",
    "Locksmith": "Cerrajería",
    "Mechanical Repair": "Mecánica",
    "Medicine": "Medicina",
    "Natural World": "Naturaleza",
    "Navigate": "",
    "Occult": "Ciencias ocultas",
    "Operate Heavy Machine": "Conducir maquinaria",
    "Persuade": "Persuasión",
    "Pilot": "",
    "Psychology": "Psicología",
    "Psychoanalysis": "Psicoanálisis",
    "Ride": "",
    "Science": "",
    "Sleight of Hand": "Juego de manos",
    "Spot Hidden": "Descubrir",
    "Stealth": "Sigilo",
    "Survival": "",
    "Swim": "Nadar",
    "Throw": "Lanzar",
    "Track": "Seguir rastros",
    # This are workarounds
    "Handgun": "Armas de fuego (Arma corta)",
    "Operate Heavy Machinery": "Conducir maquinaria",
    "Rifle": "Armas de fuego (Fusil/Escopeta)",
    "Shotgun": "Armas de fuego (Fusil/Escopeta)"
}

response = requests.get(DONJON_URL, headers=HEADERS)
random_pj = json.loads(response.text)


def fix_education(old):
    if old < 18:
        return old * 5
    else:
        return min(99, old + 72)


with open('./fvtt-Actor-chet-anderson.json', 'r', encoding='utf8') as f:
    pj = json.load(f)
    
    pj['name'] = random_pj.get('name')
    pj['token']['name'] = random_pj.get('name').split(' ')[0]
    pj['img'] = f"https://donjon.bin.sh{random_pj.get('image')}"
    
    pj['data']['characteristics']['str']['value'] = int(random_pj.get('strength')) * 5
    pj['data']['characteristics']['con']['value'] = int(random_pj.get('constitution')) * 5
    pj['data']['characteristics']['siz']['value'] = int(random_pj.get('size')) * 5
    pj['data']['characteristics']['dex']['value'] = int(random_pj.get('dexterity')) * 5
    pj['data']['characteristics']['app']['value'] = int(random_pj.get('appearance')) * 5
    pj['data']['characteristics']['int']['value'] = int(random_pj.get('intelligence')) * 5
    pj['data']['characteristics']['pow']['value'] = int(random_pj.get('power')) * 5
    pj['data']['characteristics']['edu']['value'] = fix_education(int(random_pj.get('education')))

    pj['data']['attribs']['hp']['value'] = int(random_pj.get('hit_points'))

    pj['data']['infos']['occupation'] = random_pj.get('occupation')
    pj['data']['infos']['sex'] = random_pj.get('gender')

    
    for bio in pj['data']['biography']:
        if bio['title'] == 'Descripción personal':
            bio['value'] = random_pj.get('background')
        elif bio['title'] == 'Encuentros con entidades extrañas':
            bio['value'] = random_pj.get('dilemma')

    fvtt_skills_sheet_gen = (item for item in pj['items'] if item['type'] == 'skill')
    fvtt_skills_sheet = list(fvtt_skills_sheet_gen)

    donjon_skills_list = [
        (re.search(REGEX_SKILL, s.strip()).group(1), re.search(REGEX_SKILL, s).group(3)) for s in random_pj.get('skills').split(',')
    ]
    print(donjon_skills_list)

    for eng_skill_name, eng_skill_value in donjon_skills_list:
        spa_skill_name = SKILLS.get(eng_skill_name, 'NA')

        if spa_skill_name != 'NA':
            for item in fvtt_skills_sheet:
                if spa_skill_name == item['name']:
                    print(f"{spa_skill_name}: {item['data']['value']} -> {eng_skill_value}")
                    item['data']['value'] = eng_skill_value
                    break
        else:
            print(f'ERROR: {eng_skill_name} not found in skill list')


    output_filename = f"fvtt-Actor-{random_pj.get('name').lower().replace(' ', '-')}.json"
    with open(output_filename, 'w', encoding='utf8') as fp:
        json.dump(pj, fp, indent=4, ensure_ascii=False)